CEPH S3
=======

To log into the CEPH control pod:

    kubectl exec -it -n rook $(kubectl get pods -n rook --selector=app=rook-ceph-tools --output=jsonpath={.items..metadata.name}) -- bash

Adding Users
------------

Once logged into the CEPH control pod, run the command to add a user:

    radosgw-admin --rgw-realm=nautiluss3 --rgw-zone=nautiluss3 --rgw-zonegroup=nautiluss3 user create --uid <uid> --display-name "<email>"

The `access_key` and `secret_key` is in the output from the above command.

If the request is from Matrix support channel, use the user's nickname as \<uid\>, and email address as \<email\>. 

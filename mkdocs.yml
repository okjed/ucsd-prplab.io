site_url: https://ucsd-prp.gitlab.io
site_dir: public
site_name: Nautilus documentation
repo_url: https://gitlab.com/ucsd-prp/ucsd-prp.gitlab.io
extra:
  analytics:
    provider: google
    property: G-9HWTP55HH0
markdown_extensions:
  - tables
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences  
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
plugins:
  - search
extra_css:
  - stylesheets/extra.css
theme:
  name: material
  logo: assets/Nautilus.png
  features:
    - navigation.sections
    - navigation.top
  palette: 
    - scheme: default
      toggle:
        icon: material/weather-sunny
        name: Switch to dark mode
    - scheme: slate 
      toggle:
        icon: material/weather-night
        name: Switch to light mode
nav:
    - Need help?: 'index.md'
    - 'User Guide':
        - 'Start':
            - 'Get Access': 'userdocs/start/get-access.md'
            - 'Quick Start': 'userdocs/start/quickstart.md'
            - 'Policies': 'userdocs/start/policies.md'
            - 'Deployed projects': 'userdocs/start/resources.md'
            - 'FAQ': 'userdocs/start/faq.md'
            - 'Contact in [Matrix]': 'userdocs/start/contact.md'
        - 'Tutorial':
            - 'Docker and containers': 'userdocs/tutorial/docker.md'
            - 'Basic kubernetes': 'userdocs/tutorial/basic.md'
            - 'Scaling and exposing': 'userdocs/tutorial/basic2.md'
            - 'Scheduling': 'userdocs/tutorial/scheduling.md'
            - 'Batch jobs': 'userdocs/tutorial/jobs.md'
            - 'Images': 'userdocs/tutorial/images.md'
            - 'Storage': 'userdocs/tutorial/storage.md'
        - 'Running':
            - 'Beginner start':
                - 'GPU pods': 'userdocs/running/gpu-pods.md'
                - 'Long idle pods': 'userdocs/running/long-idle.md'
                - 'ML/Jupyter pod': 'userdocs/running/jupyter.md'
                - 'Monitoring': 'userdocs/running/monitoring.md'
            - 'Running batch jobs': 'userdocs/running/jobs.md'
            - 'Intermediate':
                - 'Client scripts': 'userdocs/running/scripts.md'
                - 'Exposing HTTP': 'userdocs/running/ingress.md'
                - 'Special use': 'userdocs/running/special.md'
                - 'Faster images download': 'userdocs/running/fast-img-download.md'
                - 'Globus-connect': 'userdocs/running/globus-connect.md'
                - 'Python k8s API': 'userdocs/running/KubernetesAPI.md'
                - 'Virtualization': 'userdocs/running/virtualization.md'
                - 'Federation': 'userdocs/running/federation.md'
                - 'GUI Desktop': 'userdocs/running/gui-desktop.md'
                - 'Images we provide': 'userdocs/running/sci-img.md'
            - 'Performance':
                - 'High I/O jobs': 'userdocs/running/io-jobs.md'
                - 'CPU throttling': 'userdocs/running/cpu-throttling.md'
        - 'Storage':
            - 'Storage options':
                - 'Main storage': 'userdocs/storage/ceph-posix.md'
                - 'Ceph S3': 'userdocs/storage/ceph-s3.md'
                - 'FUSE': 'userdocs/storage/fuse.md'
                - 'Local': 'userdocs/storage/local.md'
                - 'Nextcloud': 'userdocs/storage/nextcloud.md'
                - 'SeaweedFS': 'userdocs/storage/seaweedfs.md'
                - 'Syncthing': 'userdocs/storage/syncthing.md'
            - 'Managing':
                - 'Moving data': 'userdocs/storage/move-data.md'
                - 'Purging': 'userdocs/storage/purging.md'
        - 'Development':
            - 'Building in GitLab': 'userdocs/development/gitlab.md'
            - 'Private repo': 'userdocs/development/private-repos.md'
            - 'Deploy jupyterhub': 'userdocs/development/jupyterhub.md'
            - 'Integrate GitLab and kubernetes': 'userdocs/development/k8s-integration.md'
    - 'Admin guide':
        - 'Participating':
            - 'Networking': 'admindocs/participating/network.md'
            - 'Joining a server': 'admindocs/participating/new-contributor-guide.md'
        - 'Perfsonar':
            - 'Install': 'admindocs/perfsonar/install.md'
            - 'Maddash': 'admindocs/perfsonar/maddash.md'
        - 'FIONA':
            - 'Builds': 'admindocs/fiona/builds.md'
            - 'Install': 'admindocs/fiona/install.md'
        - 'Storage':
            - 'Ceph S3': 'admindocs/storage/ceph-s3.md'
        - 'Vault':
            - 'Getting certs': 'admindocs/vault/cert.md'
        - 'Links':
            - 'Hardware': 'admindocs/links/hardware.md'
            - 'Knowledge base': 'admindocs/links/knowledge-base.md'
            - 'Software': 'admindocs/links/software.md'
            - 'Vis': 'admindocs/links/vis.md'
        - 'Cluster admin':
            - 'Cluster user management': 'admindocs/cluster/cluster-user-mgmt.md'
            - 'Cluster node management': 'admindocs/cluster/node-mgmt.md'
            - 'JupyterLab': 'admindocs/cluster/jupyterlab-admin.md'
